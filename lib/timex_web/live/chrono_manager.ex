defmodule TimexWeb.ChronoManager do
    use GenServer

    def init(ui) do
        :gproc.reg({:p, :l, :ui_event})
        {
            :ok, 
            %{
                ui_pid: ui, 
                st: :paused, 
                mode: :time, 
                count: ~T[00:00:00.00]
            }
        }
    end

    def handle_info(:"top-left", %{ui_pid: ui, mode: :time} =state) do
        GenServer.cast(ui, {:set_time_display, "00:00.00" })
        {:noreply, state |> Map.put(:mode, :chrono)}
    end

    def handle_info(:"top-left", %{ui_pid: ui, mode: :chrono} =state) do
        {:noreply, state |> Map.put(:mode, :time)}
    end
   
    def handle_info(:"bottom-right", %{ui_pid: ui, st: :paused} =state) do
        Process.send_after(self(), :tick, 10) 
        # IO.inspect Map.get(state, :st)
        {:noreply, state |> Map.put(:st, :counting)}
    end

    def handle_info(:"bottom-right", %{ui_pid: ui, mode: :chrono, st: :counting} =state) do
        # IO.inspect Map.get(state, :st)
        {:noreply, state |> Map.put(:st, :paused)}
    end

    def handle_info(:tick, %{ui_pid: ui, count: count, mode: mode, st: :counting} = state) do
        Process.send_after(self(), :tick, 10)
        count = Time.add(count,1)
        GenServer.cast(ui, {:set_time_display, Time.truncate(count, :second) |> Time.to_string })
        # IO.inspect count
        {:noreply, state |> Map.put(:count, count) }
    end

    def handle_info(:"bottom-left", %{ui_pid: ui, mode: :chrono, count: count} =state) do
        GenServer.cast(ui, {:set_time_display, "00:00.00" })
        {:noreply, state |> Map.put(:count, ~T[00:00:00.00])}
    end

    def handle_info(event, state), do: {:noreply, state}
end